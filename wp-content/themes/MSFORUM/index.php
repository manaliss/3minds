<?php /**
 * The Template for displaying all pages
 *
 * @package WordPress
 * @subpackage MSFORUM
 * @since MSFORUM 1.0
 */ ?>
<?php get_header(); ?> 
<div id="templatemo-<?php strtolower(the_title());?>">
            <div class="container">
                <div class="row">
  <div class="templatemo-line-header head_contact">
                        <div class="text-center">
                            <hr class="team_hr team_hr_left hr_gray"/><span class="txt_darkgrey"><?php the_title();?></span>
                            <hr class="team_hr team_hr_right hr_gray"/>
                        </div>
                    </div>
            <?php 
            if ( have_posts() ) : while ( have_posts() ) : the_post();
    
                get_template_part( 'content', get_post_format() );
  
            endwhile; endif; 
            ?>
</div>
</div>
</div>
<?php get_footer(); ?>