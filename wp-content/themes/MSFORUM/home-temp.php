<?php /* Template Name: Home */ ?>

<?php get_header();?>
        
        <divload>
            <!-- Carousel -->
            <div id="templatemo-carousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
          
                <div class="carousel-inner">
                    <div class="item active">
                        <div class="container">
                            <div class="carousel-caption">
                                <h1>WELCOME TO URBANIC</h1>
                                <p>CHOOSE THE ONE SUITABLE FOR YOU</p>
                                <p><a class="btn btn-lg btn-green" href="#" role="button" style="margin: 20px;">View Demo</a> 
                                	<a class="btn btn-lg btn-orange" href="#" role="button">Free Download</a></p>
                            </div>
                        </div>
                    </div>
                </div>              
        </div>

        <div class="templatemo-welcome" id="templatemo-welcome">
            <div class="container">
                <div class="templatemo-slogan text-center">
                    <span class="txt_darkgrey">Welcome to </span><span class="txt_orange">Urbanic Design</span>
                    <p class="txt_slogan"><i>Lorem ipsum dolor sit amet, consect adipiscing elit. Etiam metus libero mauriec ignissim fermentum.</i></p>
                </div>	
            </div>
        </div>
        
        <div class="templatemo-service">
            <div class="container">
                <div class="row">
                <?php $blog_posts_args = array('post_type' => 'post', 'post_per_page' =>4, 'category' => 3,  'order' => 'DESC');
                $blog_post=get_posts($blog_posts_args);
                if ( $blog_post ) {
                    foreach ( $blog_post as $post ) :
                        setup_postdata( $post );
                        ?>
                    <div class="col-md-3">
                        <div class="templatemo-service-item">
                            <div>
                               <?php
                               if ( has_post_thumbnail() ) {
                                        the_post_thumbnail(array(250,150));
                                    }?>
                                <span class="templatemo-service-item-header"><a href="<?php the_permalink();?>"><?php the_title();?></a></span>
                            </div>
                            <p><?php the_excerpt();?></p>
                            <div class="text-center">
                            	<a href="<?php the_permalink();?>" 
                                	class="templatemo-btn-read-more btn btn-orange">READ MORE</a>
                            </div>
                            <br class="clearfix"/>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <?php
                        endforeach; 
                        wp_reset_postdata();
                    }?>

                     <div class="clearfix"></div>
                <div class="row text-center">
                    <a class="btn_loadmore btn btn-lg btn-orange" href="<?php echo home_url().'/blog';?>" role="button">VIEW ALL</a>
                </div>
                </div>
            </div>
        </div>


        <div id="templatemo-portfolio" >
            <div class="container">
                <div class="row">
                    <div class="templatemo-line-header" >
                        <div class="text-center">
                            <hr class="team_hr team_hr_left hr_gray"/><span class="txt_darkgrey">OUR PORTFOLIO</span>
                            <hr class="team_hr team_hr_right hr_gray" />
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="templatemo-gallery-category" style="font-size:16px; margin-top:80px;">
                        <div class="text-center">
                            <a class="active" href=".gallery">All</a> / <a href=".gallery-design">Web Design </a>/ <a href=".gallery-graphic">Graphic</a> / <a href=".gallery-inspiration">Inspiration</a> / <a href=".gallery-creative">Creative</a>							
                        </div>
                    </div>
                </div> <!-- /.row -->


                <div class="clearfix"></div>
                <div class="text-center">
                    <ul class="templatemo-project-gallery" >
                        <li class="col-lg-2 col-md-2 col-sm-2  gallery gallery-graphic" >
                            <a class="colorbox" href="<?php echo get_template_directory_uri();?>/images/full-gallery-image-1.jpg" data-group="gallery-graphic">
                                <div class="templatemo-project-box">

                                    <img src="<?php echo get_template_directory_uri();?>/images/gallery-image-1.jpg" class="img-responsive" alt="gallery" />

                                    <div class="project-overlay">
                                        <h5>Graphic</h5>
                                        <hr />
                                        <h4>TEA POT</h4>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="col-lg-2 col-md-2 col-sm-2  gallery gallery-creative" >
                            <a class="colorbox" href="<?php echo get_template_directory_uri();?>/images/full-gallery-image-2.jpg" data-group="gallery-creative">
                                <div class="templatemo-project-box">
                                    <img src="<?php echo get_template_directory_uri();?>/images/gallery-image-2.jpg" class="img-responsive" alt="gallery" />
                                    <div class="project-overlay">
                                        <h5>Creative</h5>
                                        <hr />
                                        <h4>BREAKFAST</h4>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="col-lg-2 col-md-2 col-sm-2  gallery gallery-inspiration" >
                            <a class="colorbox" href="<?php echo get_template_directory_uri();?>/images/full-gallery-image-3.jpg" data-group="gallery-inspiration">
                                <div class="templatemo-project-box">
                                    <img src="<?php echo get_template_directory_uri();?>/images/gallery-image-3.jpg" class="img-responsive" alt="gallery" />
                                    <div class="project-overlay">
                                        <h5>Inspiration</h5>
                                        <hr />
                                        <h4>GREEN COLORS</h4>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="col-lg-2 col-md-2 col-sm-2  gallery gallery-design" >
                            <a class="colorbox" href="<?php echo get_template_directory_uri();?>/images/full-gallery-image-4.jpg" data-group="gallery-design">
                                <div class="templatemo-project-box">
                                    <img src="<?php echo get_template_directory_uri();?>/images/gallery-image-4.jpg" class="img-responsive" alt="gallery" />
                                    <div class="project-overlay">
                                        <h5>Web Design</h5>
                                        <hr />
                                        <h4>CAMERA</h4>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="col-lg-2 col-md-2 col-sm-2  gallery gallery-inspiration" >
                            <a class="colorbox" href="images/full-gallery-image-5.jpg" data-group="gallery-inspiration">
                                <div class="templatemo-project-box">
                                    <img src="<?php echo get_template_directory_uri();?>/images/gallery-image-5.jpg" class="img-responsive" alt="gallery" />
                                    <div class="project-overlay">
                                        <h5>Inspiration</h5>
                                        <hr />
                                        <h4>PLANT</h4>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="col-lg-2 col-md-2 col-sm-2  gallery gallery-inspiration" >
                            <a class="colorbox" href="<?php echo get_template_directory_uri();?>/images/full-gallery-image-6.jpg" data-group="gallery-inspiration">
                                <div class="templatemo-project-box">
                                    <img src="<?php echo get_template_directory_uri();?>/images/gallery-image-6.jpg" class="img-responsive" alt="gallery" />
                                    <div class="project-overlay">
                                        <h5>Inspiration</h5>
                                        <hr />
                                        <h4>CABLE TRAIN</h4>
                                    </div>
                                </div>
                            </a>
                        </li>
                        
                        <li class="col-lg-2 col-md-2 col-sm-2 gallery gallery-design" >
                            <a class="colorbox" href="<?php echo get_template_directory_uri();?>/images/full-gallery-image-7.jpg" data-group="gallery-design">
                                <div class="templatemo-project-box">
                                    <img src="<?php echo get_template_directory_uri();?>/images/gallery-image-7.jpg" class="img-responsive" alt="gallery" />
                                    <div class="project-overlay">
                                        <h5>Web Design</h5>
                                        <hr />
                                        <h4>CITY</h4>
                                    </div>
                                </div>
                            </a>
                        </li>
                        
                        <li class="col-lg-2 col-md-2 col-sm-2 gallery gallery-creative" >
                            <a class="colorbox" href="<?php echo get_template_directory_uri();?>/images/full-gallery-image-8.jpg" data-group="gallery-creative">
                                <div class="templatemo-project-box">
                                    <img src="<?php echo get_template_directory_uri();?>/images/gallery-image-8.jpg" class="img-responsive" alt="gallery" />
                                    <div class="project-overlay">
                                        <h5>Creative</h5>
                                        <hr />
                                        <h4>BIRDS</h4>
                                    </div>
                                </div>
                            </a>
                        </li>
                        
                        <li class="col-lg-2 col-md-2 col-sm-2 gallery gallery-graphic" >
                            <a class="colorbox" href="<?php echo get_template_directory_uri();?>/images/full-gallery-image-9.jpg" data-group="gallery-graphic">
                                <div class="templatemo-project-box">
                                    <img src="<?php echo get_template_directory_uri();?>/images/gallery-image-9.jpg" class="img-responsive" alt="gallery" />
                                    <div class="project-overlay">
                                        <h5>Graphic</h5>
                                        <hr />
                                        <h4>NATURE</h4>
                                    </div>
                                </div>
                            </a>
                        </li>
                        
                        <li class="col-lg-2 col-md-2 col-sm-2 gallery gallery-inspiration" >
                            <a class="colorbox" href="<?php echo get_template_directory_uri();?>/images/full-gallery-image-10.jpg" data-group="gallery-inspiration">
                                <div class="templatemo-project-box">
                                    <img src="<?php echo get_template_directory_uri();?>/images/gallery-image-10.jpg" class="img-responsive" alt="gallery" />
                                    <div class="project-overlay">
                                        <h5>Inspiration</h5>
                                        <hr />
                                        <h4>DOGGY</h4>
                                    </div>
                                </div>
                            </a>
                        </li>

                    </ul><!-- /.gallery -->
                </div>
               
            </div><!-- /.container -->
        </div> <!-- /.templatemo-portfolio -->


          <div id="templatemo-blog">
            <div class="container">
                <div class="row">
                    <div class="templatemo-line-header" style="margin-top: 0px;" >
                        <div class="text-center">
                            <hr class="team_hr team_hr_left hr_gray"/><span class="span_blog txt_darkgrey">Products</span>
                            <hr class="team_hr team_hr_right hr_gray" />
                        </div>
                    </div>
                    <br class="clearfix"/>
                </div> 
                <div class="blog_box">
                <?php $blog_posts_args = array('post_type' => 'product', 'post_per_page' =>4, 'order' => 'DESC');
                $blog_post=get_posts($blog_posts_args);
                if ( $blog_post ) {
                    foreach ( $blog_post as $post ) :
                        setup_postdata( $post );
                        ?>
                    <div class="col-sm-5 col-md-6 blog_post">
                        <ul class="list-inline">
                            <li class="col-md-4">
                                <a href="<?php the_permalink();?>">
                                <?php  if ( has_post_thumbnail() ) {
                                        the_post_thumbnail(array(100,80));
                                    }?>
                                   <!--  <img class="img-responsive" src="images/blog-image-1.jpg" alt="gallery 1" /> -->
                                </a>
                            </li>
                            <li  class="col-md-8">
                                <div class="pull-left">
                                    <span class="blog_header"><?php the_title();?></span><br/>
                                   <?php $postcat = get_the_category( $post->ID );
if ( ! empty( $postcat ) ) { ?>
<span>Category : <span class="txt_orange"><?php echo esc_html( $postcat[0]->name ); ?></span></span><?php }?><br>
<?php $price=get_post_meta(get_the_ID(),'price',true);
$unit = get_post_meta(get_the_ID(),'price_unit',true);
if($price){
?>
<span>Price : <span class="txt_orange"><?php echo $unit.esc_html( $price ); ?></span></span><?php }?>

                                </div>
                                <div class="pull-right">
                                    <a class="btn btn-orange" href="<?php the_permalink();?>" role="button">Detail</a>
                                </div>
                                <div class="clearfix"> </div>
                                <p class="blog_text">
                                    <?php the_excerpt();?>
                                </p>
                            </li>
                        </ul>
                    </div> <!-- /.blog_post 1 -->
                                   <?php
                endforeach; 
                wp_reset_postdata();
                    }?>                                
                  <div class="templatemo_clear"></div>
                                      
                </div>
            </div>
        </div>

        <div class="templatemo-tweets">
        <div class="templatemo-line-header" >
                        <div class="text-center">
                            <hr class="team_hr team_hr_left hr_gray"/><span class="txt_darkgrey">Tweeter Feeds</span>
                            <hr class="team_hr team_hr_right hr_gray" />
                        </div>
                    </div>
                    <div class="clearfix"></div>
            <div class="container">
                <div class="row" style="margin-top:20px;">
                 
                        <?php echo do_shortcode('[custom-twitter-feeds]');?>
                 </div><!-- /.row -->
            </div><!-- /.container -->
        </div>

<?php get_footer();?>