<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header(); ?>
<div id="templatemo-<?php strtolower(the_title());?>">
            <div class="container">
                <div class="row">
  <div class="templatemo-line-header head_contact">
                        <div class="text-center">
                            <hr class="team_hr team_hr_left hr_gray"/><span class="txt_darkgrey"><?php the_title();?></span>
                            <hr class="team_hr team_hr_right hr_gray"/>
                        </div>
                    </div>
			<?php
				// Start the Loop.
			while ( have_posts() ) :
				the_post();
				
				get_template_part( 'content', get_post_format() );
				// Previous/next post navigation.
				
				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) {
					comments_template();
				}
				endwhile;
			?>
</div>
</div>
</div>
<?php

get_footer();