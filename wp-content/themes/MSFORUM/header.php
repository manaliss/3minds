<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php  bloginfo( 'name' ); ?></title>
        <meta name="keywords" content="" />
		<meta name="description" content="" />

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--<link rel="shortcut icon" href="PUT YOUR FAVICON HERE">-->
        
        <!-- Google Web Font Embed -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<?php wp_head();?>
    </head>
    
    <body <?php body_class();?>>

        <div class="templatemo-top-bar" id="templatemo-top">
            <div class="container">
                <div class="subheader">
                    <div id="phone" class="pull-left">
                            <img src="<?php echo get_template_directory_uri();?>/images/phone.png" alt="phone"/>
                            090-080-0110
                    </div>
                    <div id="email" class="pull-right">
                            <img src="<?php echo get_template_directory_uri();?>/images/email.png" alt="email"/>
                            info@company.com
                    </div>
                </div>
            </div>
        </div>
        <div class="templatemo-top-menu">
            <div class="container">
                <!-- Static navbar -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">
                        <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                </button>
                               <?php $custom_logo_id = get_theme_mod( 'custom_logo' );
                                $image = wp_get_attachment_image_src( $custom_logo_id , 'full' );?>
                                <a href="<?php echo home_url();?>" class="navbar-brand"><img src="<?php echo $image[0]; ?>" alt="Urbanic Template" title="Urbanic Template" /></a>
                        </div>

                <?php $menu_args = array('theme_location' => 'primary_menu', 'container' => 'div', 'container_class' => 'navbar-collapse collapse', 'container_id' => 'templatemo-nav-bar', 'menu_class' => 'nav navbar-nav navbar-right', 'menu_id' => '');
                       wp_nav_menu($menu_args);?>
                    
                    </div><!--/.container-fluid -->
                </div><!--/.navbar -->
            </div> <!-- /container -->
        </div>