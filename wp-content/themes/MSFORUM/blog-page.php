<?php 
/* Template Name: Blog */?>
<?php get_header(); ?> 
    <div class="templatemo-service">
            <div class="container">
                <div class="row">
<?php $blog_posts_args = array('post_type' => 'post', 'category' => 3,
        'order' => 'DESC');
$blog_post=get_posts($blog_posts_args);
if ( $blog_post ) {
    foreach ( $blog_post as $post ) :
        setup_postdata( $post );
        ?>
                    <div class="col-md-3">
                        <div class="templatemo-service-item">
                            <div>
                               <?php
                               if ( has_post_thumbnail() ) {
                                        the_post_thumbnail(array(250,150));
                                    }?>
                                <span class="templatemo-service-item-header"><a href="<?php the_permalink();?>"><?php the_title();?></a></span>
                            </div>
                            <p><?php the_excerpt();?></p>
                            <div class="text-center">
                            	<a href="<?php the_permalink();?>" 
                                	class="templatemo-btn-read-more btn btn-orange">READ MORE</a>
                            </div>
                            <br class="clearfix"/>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <?php
    endforeach; 
    wp_reset_postdata();
}?>

                 </div>
            </div>
        </div>

<?php get_footer(); ?>