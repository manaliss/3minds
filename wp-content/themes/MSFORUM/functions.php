<?php
//register menu for theme
if ( ! function_exists( 'theme_register_nav_menu' ) ) {
 
    function theme_register_nav_menu(){
        register_nav_menus( array(
            'primary_menu' => __( 'Primary Menu', 'msforum' ),
            
        ) );
    }
    add_action( 'after_setup_theme', 'theme_register_nav_menu', 0 );
}


//adding theme supports

add_theme_support( 'post-thumbnails' );
add_theme_support( 'custom-logo', array(
	'height'      => 100,
	'width'       => 400,
	'flex-height' => true,
	'flex-width'  => true,
	'header-text' => array( 'site-title', 'site-description' ),
) );


//custom post type products
function reg_my_product_custom_posttype() {
    register_post_type( 'product',
        array(
            'labels' => array(
                'name' => 'Products',
                'singular_name' => 'Product',
                'add_new' => 'Add New',
                'add_new_item' => 'Add New Product',
                'edit' => 'Edit',
                'edit_item' => 'Edit Product',
                'new_item' => 'New Product',
                'view' => 'View',
                'view_item' => 'View Product',
                'search_items' => 'Search Products',
                'not_found' => 'No Products found',
                'not_found_in_trash' => 'No Products found in Trash',
                'parent' => 'Parent Product'
            ),
 
            'public' => true,
            'menu_position' => 15,
            'supports' => array( 'title', 'editor', 'thumbnail' ),
            'taxonomies' => array( 'category' ),
            'has_archive' => true
        )
    );
}
add_action('init','reg_my_product_custom_posttype');



add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );
function add_theme_scripts() {
     wp_enqueue_script( 'script', get_template_directory_uri() . '/js/jquery.min.js', array ( 'jquery' ));
     //wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array ( 'jquery' ));
     wp_enqueue_script( 'stickUp', get_template_directory_uri() . '/js/stickUp.min.js', array ( 'jquery' ));
     wp_enqueue_script( 'colorbox', get_template_directory_uri() . '/js/colorbox/jquery.colorbox-min.js', array ( 'jquery' ));
     wp_enqueue_script( 'templatemo_script', get_template_directory_uri() . '/js/templatemo_script.js', array ( 'jquery' ));
  

  wp_enqueue_style( 'bootstrap_css', get_template_directory_uri() . '/css/bootstrap.css', array());
  wp_enqueue_style( 'colorbox_css', get_template_directory_uri() . '/js/colorbox/colorbox.css', array());
  wp_enqueue_style( 'templatemo_style', get_template_directory_uri() . '/css/templatemo_style.css', array());

}