<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', '3minds');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'K>GzhYX_]g,Moy<fAjKL$b7t_{O}(m@7*D.ob%(M8Wj]!5n6aiIa<3VWe?fW(irO');
define('SECURE_AUTH_KEY',  'CYkH&t) Hn?MSiTI.CiV6bh+w+<TU`b<sCP)`t;nd$ui|bM<9rFgQhUF|^bzyG8b');
define('LOGGED_IN_KEY',    '}5%pq%KGX>B?oT;Eq%nB~NMmK|Ep6o>hoEgU#NypBF@@xU|e~0^vTymSj[zQ_[c0');
define('NONCE_KEY',        'SC|m~5&-fP`^kP0|#w2Feq{bMbXMk{<F6oR<K/DhfA&M98W6T52DKqa=z1|O5&.l');
define('AUTH_SALT',        'mzOzBoe2fF]0f>SFP`@V))MLt`Y%/Mz8RjRi3CE[T~8;gyh;zM3s8-Y6u5,D5L$-');
define('SECURE_AUTH_SALT', '!~6If/88{tloq|qnSu[*y>L;H~26D2tb^and-s97B]11XzNDyM++mN5^r{&;ra]K');
define('LOGGED_IN_SALT',   '_j9]v@zR_){!>l3[[V6czit7-58h+|*[ra#{>J_IB;p{5doY1}vu&Vwb2Cz04^?7');
define('NONCE_SALT',       '$m:,v)Bfj6v3XQdwTrep7cD^Vr*yAVR$:> bb39@W<UZ|w[n9izem_/slf+9eUFv');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
